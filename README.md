# Open Source GRC

## Project Purpose

Governance Risk and Compliance (GRC) programs are either too expensive for the value they provide, too expensive for early-stage companies to adopt while building and growing their security programs, or they just really suck. Open Source GRC is meant to be either a fully open source or an open core GRC tool that is built and maintained by a community of people who want a more powerful/flexible/less-terrible GRC solution.

## What this tool aims to be:
* Flexible enough that the tool can adapt to ~80% of organization's workflows (instead of organizations needing to adapt their workflows to the tool)
* Structured enough that you can basic guardrails in place in case you're scaling your security program without significant GRC experience
* Community driven
* A tool that offers as many GRC best practices for free as possible
   * (No one should have to pay $30k for basic advice on how to get started with a strong GRC program)
* API-first to enable an ecosystem of tool/system connections and automation to emerge as it grows
* A tool that brings as much **good** data and objectivity to security compliance as possible
   * (Let's get rid of the fluff in compliance)

## What this tool is NOT meant to be:
* Snake oil
   * There are too many tools out there that focus on the pretty dashboards and their marketing/sales more than they focus on the actual benefit they bring to an organization's security program
* Pay to play
   * While it's possible that it will be necessary/appropriate to move to an open core model as this tool grows in order to incentivize development and support activities, no Series-A to Series-C company should have to pay for the basic functionality of this tool. This is the boring stuff and boring stuff should be free.
* Everything to everyone
   * This tool will be a snapshot representation of the community consensus for the best way to manage a GRC program. This tool and the guardrails it enforces won't match the way every organization wants to structure their program, and this should be fine. There's lots of tools and if we try and make this the market-dominant solution it will just lead to so much complexity and ambiguous workflows that it will only be useful to people with advanced knowledge of GRC programs. If you're an industry leader and you have a large team of GRC analysts to manage your program then why are you using this tool? Just go pay for Archer instead.

## How to use this project:
This project is in its infancy and the first things to be built out will be a product Epic (link to be added) which will have sub-epics linked that will describe each object planned for the tool.

Once we kick this off and build the basic structure then it will be easier to submit MR's to the code. In the mean time please feel free to submit an issue(link to be added) and I will be sure to respond.

## Copyright

Added the MIT license based on some cursory research.
