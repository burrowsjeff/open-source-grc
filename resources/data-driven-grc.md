# Goal

This document is meant to provide a guide to the "philosophy" behind the type of GRC program that the [Open Source GRC tool](/../README.md) in intended to serve. There's no singular correct approach to GRC, but there are 2 primary models for Security GRC that I've seen in my career:

1. **The advisor model:** This is the older style of GRC where the primary goal of the program is to achieve certifications and when you ask for clarity from the people involved you're likely to just get the requirements themselves spit back at you without a lot of translation or synthesizing of information involved.
2. **The data-driven model:** This is the more modern approach to Security GRC that aims to have certifications and attestations as an output, but the program itself is really designed around enabling businesses to appropriately address risk and make data-driven decisions about how to address that risk (without trying to eliminate risk which is impossible).

If it's not clear from the biased framing above, this Open Source GRC project is built around the second model.

# What does it mean to "enable business"?

This just means that security for the sake of security isn't the goal. Appropriately addressing risk while still accomplishing organizational objectives is the goal. The difference is that if you're genuinely just trying to reduce security risk at all costs, you'll unplug your product from the internet and stick it in a closet somewhere so no one can possibly attack it.

>
> Compliance is a natural output of good security. Good security absolutely **does not** follow good compliance.
>

The main point here is that refusing to patch a critical vulnerability when there's no measurable impact to customers is a bad decision. Delaying patching a vulnerability when it would mean a 30% churn in enterprise customers because it breaks core product functionality is a very rational decision. The goal of enabling business is to provide the data needed to make that decision as an informed leader. 

# This sounds great, but HOW do you do this?

The short answer is "detailed testing". The advisor model tends to view certifications and attestations as the end goal of the program. If you let that be the goal then you're letting these crappy standards dictate your security practices instead of treating certifications and attestations as one of the natural outputs of the real work. In order for data to be a primary output of your GRC program, then you need a detailed understanding of:

1. What security controls/objectives you care about given the context of your product and the certifications your customers care about
2. An understanding of how each of those controls are operating across all of the tools/systems/services/environments of your organization
   * i.e. You don't really care how the nebulous idea of "least privilege" is operating across your entire organization, you care about how least privilege is operating within Okta as compared to AWS

This is a pretty boring solution to a complex problem, but it works. The data-driven GRC model takes control testing as the backbone of collecting this information. It brings a consistent methodology to measuring the operation and maturity of all your security controls across all parts of your tech stack in a way that makes re-packing that information really easy for all the various program outputs like:

* Delivering reliable data about where you hold risk to your board of directors
* Easily slicing the sub-set of information that's relevant to a SOC 2 Type 2 audit so these audits are easy without dictating how you deploy your security requirements
* Provide information to the system owners of your tech stack so they know how their systems are operating from a security perspective and what are the most impactful changes they could make with limited resources
* Meaningful audit findings that don't say useless things like "you should invest more in least privilege" and instead say actionable things like "80% of all access to GitHub is using this singular role. You can reduce pervasive access if you break that into 2 separate roles; 1 for the ability to create PRs and the other with the ability to create PRs and approve PRs"