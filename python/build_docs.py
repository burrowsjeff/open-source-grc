from grc.markdown_parser import RenderingCollection
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Parse markdown files and generate control mappings.")
    parser.add_argument("control_path", type=str, nargs='?', help="Path to the folder containing markdown files.")
    parser.add_argument("output_path", type=str, help="Path to the output CSV file.")
    args = parser.parse_args()
    rc = RenderingCollection()
    rc.parse_folder(args.control_path)
    rc.write_to_html(args.output_path)
