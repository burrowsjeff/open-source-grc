#!/usr/bin/env python

import requests
import json
from pathlib import Path
import jinja2
import click


#TEMPALTES_DIR = Path("nist80053/templates")

CONTROL_TEMPLATE = """
# {{ control.id | upper }}: {{ control.title }}

{% if control.params %}
## Parameters
{% for param in control.params %}
- **{{ param.id }}**: {{ param.label | default("N/A") }} (Type: {{ param.type | default("N/A") }})
{% endfor %}
{% endif %}

{% if control.parts %}
## Control Statement
{% for part in control.parts if part.name == "statement" %}
{{ part.prose }}
{% endfor %}

{% for part in control.parts if part.name == "guidance" %}
## Guidance
{{ part.prose }}
{% endfor %}
{% endif %}

{% if control.links %}
## Related Controls
{% for link in control.links %}
- [{{ link.href }}]({{ link.href }})
{% endfor %}
{% endif %}

{% if control.props %}
## Additional Information
{% for prop in control.props %}
- **{{ prop.name }}**: {{ prop.value }}
{% endfor %}
{% endif %}

"""

# Define the NIST OSCAL catalog URL (SP 800-53 as an example)
OSCAL_CATALOG_URL = "https://raw.githubusercontent.com/usnistgov/oscal-content/refs/heads/main/nist.gov/SP800-53/rev5/json/NIST_SP-800-53_rev5_catalog.json"

# Define the base directory where the structure will be created
BASE_DIR = Path("oscal_catalog")

def download_oscal_catalog(url):
    """Download the OSCAL JSON catalog from the given URL."""
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()
    else:
        raise Exception(f"Failed to download OSCAL catalog: {response.status_code}")

def create_directory_structure(data, base_dir):
    """Create directories based on groups, controls, and control parts."""
    base_dir.mkdir(parents=True, exist_ok=True)

    # Ensure catalog structure exists
    if "catalog" not in data or "groups" not in data["catalog"]:
        raise ValueError("Invalid OSCAL catalog format")

    for group in data["catalog"]["groups"]:
        process_group(group, base_dir)

def process_group(group, parent_dir):
    """Recursively process groups and controls inside them."""
    group_dir = parent_dir / group["id"]
    group_dir.mkdir(exist_ok=True)

    for control in group.get("controls", []):
        process_control(control, group_dir)

    for subgroup in group.get("groups", []):
        process_group(subgroup, group_dir)

def process_control(control, parent_dir):
    """Create a directory for each control and process its parts."""
    control_dir = parent_dir / control["id"]
    control_dir.mkdir(exist_ok=True)
    template_str = jinja2.Template(CONTROL_TEMPLATE)
    rendered_template = template_str.render(control=control)
    
    with open(control_dir / "control.md", "w") as f:
        f.write(rendered_template)


def process_part(part, parent_dir):
    """Create a directory for each control part. (Not used)"""
    part_id = part.get("id", part.get("name", "unknown_part"))
    part_dir = parent_dir / part_id
    part_dir.mkdir(exist_ok=True)

@click.command()
@click.option('--write-dir', default='output-catalog', help='Directory to write the output files')
@click.option('--cat-url', default=OSCAL_CATALOG_URL, help='URL to the OSCAL catalog JSON file')
def main(write_dir, cat_url):
    print("Downloading OSCAL catalog...")
    write_dir = Path(write_dir)
    if write_dir.exists():
        raise ValueError(f"Directory already exists: {write_dir}")
    if not write_dir.parent.exists():
        raise ValueError(f"Parent directory does not exist: {write_dir.parent}")
    
    oscal_data = download_oscal_catalog(cat_url)

    print(f"Creating directory structure in: {write_dir}")
    create_directory_structure(oscal_data, Path(write_dir))

    print("Directory structure created successfully.")

if __name__ == "__main__":
    main()