import mistune
from pathlib import Path
import json
import pandas as pd
import argparse
import logging
import jinja2
logging.basicConfig(level=logging.DEBUG)


# rule to json serialize path objects
def path_serializer(obj):
    if isinstance(obj, Path):
        return str(obj)
    raise TypeError(f"Object of type {type(obj)} is not JSON serializable")


class RenderingCollection:

    def __init__(self):

        self.renderers = {}

    def parse(self, path):
        with open(path, 'r') as f:
            text = f.read()
        renderer = GRCRenderer()
        path = Path(path)
        self.renderers[path.stem] = {
            "renderer": renderer,
            "path": path,
        }
        #self.renderers[path.stem] = renderer
        parser = mistune.create_markdown(renderer=renderer)
        rendered_string = parser(text)
        self.renderers[path.stem].update({"rendered_string": rendered_string})

    def parse_folder(self, folder_path):
        folder_path = Path(folder_path)
        for file in folder_path.glob('*.md'):
            self.parse(file)

    def info(self):
        data = {}
        for stem, renderer_info in self.renderers.items():
            renderer = renderer_info['renderer']
            path = renderer_info['path']

            if renderer.isComplete():
                info = renderer.info()
                info.update({"path": path})
                data[stem] = info
        return data
    
    def markdown(self):
        rendered = {}
        for stem, renderer_info in self.renderers.items():
            text = renderer_info['rendered_string']
            rendered[stem] = text
        return rendered

    def write_to_html(self, output_path, clobber=False):
        loader = jinja2.FileSystemLoader(searchpath=Path(__file__).parent/'templates')
        env = jinja2.Environment(loader=loader)
        index_template = env.get_template('index.html')
        mappings_template = env.get_template('mappings.html')
        markdown = self.markdown()
        output_path = Path(output_path)
        if clobber:
            
            if output_path.exists():
                for file in output_path.glob('*'):
                    if file.is_dir():
                        for subfile in file.glob('*'):
                            subfile.unlink()
                    else:
                        file.unlink()
                    
        output_path.mkdir(parents=True, exist_ok=True)
        links = {}
        for stem, md in markdown.items():
            links[stem] = f"{stem}.html"
            with open(output_path / f"{stem}.html", 'w') as f:
                f.write(md)
        
        index = index_template.render(data=links)
        with open(output_path / 'index.html', 'w') as f:
            f.write(index)
            
        info = self.info()
        with open(output_path / 'info.json', 'w') as f:
            f.write(json.dumps(info, indent=4, default=path_serializer))
        
        mappings = mappings_template.render(data=json.dumps(info, default=path_serializer))
        with open(output_path / "mappings.html", 'w') as f:
            f.write(mappings)
            
        # copy static files
        static_path = Path(__file__).parent / 'static'
        (output_path / 'static').mkdir(parents=True, exist_ok=True)
        for file in static_path.glob('*'):
            with open(output_path / 'static' / file.name, 'w') as f:
                f.write(file.read_text())

    def info_df(self):
        data = self.info()
        df = pd.DataFrame(data).T
        df = df.explode("Mappings")
        df['Mapped Control Entity'] = df['Mappings'].apply(lambda x: list(x.keys())[0])
        df['Mapped Control Sections'] = df['Mappings'].apply(lambda x: list(x.values())[0])
        df = df.groupby('Mapped Control Entity').apply(lambda x: x)
        df = df.explode("Mapped Control Sections")
        return df


class GRCRenderer(mistune.HTMLRenderer):

    def __init__(self):
        super().__init__()
        self.sections = {
            "Control Family": "",
            "Control ID": "",
        }

        self.code_blocks = []
        self.current_section = None
        self.tracked_section = None

    def render_token(self, token, state):
        Type = token["type"]
        if Type == "heading" and "children" in token and len(token["children"]) > 0:
            if token["children"][0]["raw"] in self.sections:
                self.tracked_section = token["children"][0]["raw"]
                return f"<h1>{token['children'][0]['raw']}</h1>"
            else:
                return super().render_token(token, state)
        else:
            return super().render_token(token, state)

    def heading(self, text, level, **kwargs):
        self.current_section = text
        return super().heading(text, level, **kwargs)

    def paragraph(self, text):
        if self.tracked_section is not None:
            self.sections[self.tracked_section]=text
            self.tracked_section = None

        return super().paragraph(text)

    def block_code(self, code, info=None):
        if info == "json_cmap":
            json_data = json.loads(code)
            self.code_blocks.append({self.current_section: json_data})

        return super().block_code(code, info)

    def info(self):
        data = {
            "Control Family": self.sections["Control Family"],
            "Control ID": self.sections["Control ID"],
            "Mappings": self.code_blocks,
            "IsComplete": self.isComplete()
        }
        return data
    
    def isComplete(self):
        if self.sections["Control Family"] and self.sections["Control ID"] and self.code_blocks: 
            return True
        return False

class ControlRenderer(mistune.BaseRenderer):
    def __init__(self):
        super().__init__()
        self.sections = {
            "Control Family": "",
            "Control ID": "",
        }
        
        self.code_blocks = []
        self.current_section = None
        self.tracked_section = None
    

    def heading(self, token, *args, **kwargs):
        self.current_section = token["children"][0]["raw"]
        if token["attrs"]["level"] == 1 and "children" in token and len(token["children"]) > 0:
            if token["children"][0]["raw"] in self.sections:
                self.tracked_section = token["children"][0]["raw"]
            
            else:
                self.tracked_section = None
        else:
            self.tracked_section = None
        return ""


    def paragraph(self, token, *args, **kwargs):
        if self.tracked_section is not None:
            self.sections[self.tracked_section]+=token["children"][0]["raw"]
        return ""
        
    def blank_line(self, *args, **kwargs):
        #print("we have a blank line")
        return ""
    
    def list(self, token, *args, **kwargs):
        #print(f"we have a list {token}")
        return ""
    
    def block_code(self, token, *args, **kwargs):
        if token["attrs"]["info"] == "json_cmap":
            try:
                json_data = json.loads(token["raw"])
            except json.JSONDecodeError as e:
                print(f"Error parsing json {token} {e}")
                return ""
            self.code_blocks.append({self.current_section: json.loads(token["raw"])})
        return ""
    
    def info(self):
        data = {
            "Control Family": self.sections["Control Family"],
            "Control ID": self.sections["Control ID"],
            "Mappings": self.code_blocks,
            "IsComplete": self.isComplete()
        }
        return data
    
    def isComplete(self):
        if self.sections["Control Family"] and self.sections["Control ID"] and self.code_blocks: 
            return True
        return False


def extract_sections_and_code(markdown_text):
    renderer = CustomRenderer()
    parser = mistune.create_markdown(renderer=renderer)
    parser(markdown_text)  # This triggers parsing
    return renderer.sections, renderer.code_blocks


def main(output_path, control_path):


    if control_path is None:
        control_path = Path("continuous-control-monitoring")
        
    logging.debug(f"Control path is {control_path}")
    logging.debug(f"Output path is {output_path}")
    rc = RenderingCollection()
    rc.parse_folder(control_path)
    data = rc.info_df()
    #data.to_csv("control_mapping.csv")
    mapping_template = f"""
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Control Mapping</title>
    </head>
    <body>
        <h1>Control Mapping</h1>
        <p>Below is the control mapping for the controls in the continuous control monitoring project</p>
        {data[['Control Family', 'Control ID', 'IsComplete', 'Mapped Control Sections']].to_html()}
    </body>
    </html>
    """
    
    index_template = f"""
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>GRC MAIN</title>
    </head>
    <body>
        <h1>GRC MAIN</h1>
        <a href="control_mapping.html">Control Mapping</a>
    </body>
    </html>
    """
    
    output_path = Path(output_path)
    output_path.mkdir(parents=True, exist_ok=True)
    logging.debug(f"Writing to {output_path}")    
    with open(Path(output_path) / 'index.html', 'w') as f:
        f.write(index_template)
    with open(Path(output_path) / 'control_mapping.html', 'w') as f:
        f.write(mapping_template)


#if __name__ == "__main__":
#    parser = argparse.ArgumentParser(description="Parse markdown files and generate control mappings.")
#    parser.add_argument("control_path", type=str, nargs='?', help="Path to the folder containing markdown files.")
#    parser.add_argument("output_path", type=str, help="Path to the output CSV file.")
#    args = parser.parse_args()
#    main(args.output_path, args.control_path)