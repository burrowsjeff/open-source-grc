# Control Family
Access Control

# Control ID
AC-02

# Control Statement
* Define and document the types of accounts allowed and specifically prohibited for use within the system;
* Assign account managers;
* Require [Assignment: organization-defined prerequisites and criteria] for group and role membership;
* Specify:
* Authorized users of the system;
* Group and role membership; and
* Access authorizations (i.e., privileges) and [Assignment: organization-defined attributes (as required)] for each account;
* Require approvals by [Assignment: organization-defined personnel or roles] for requests to create accounts;
* Create, enable, modify, disable, and remove accounts in accordance with [Assignment: organization-defined policy, procedures, prerequisites, and criteria];
* Monitor the use of accounts;
* Notify account managers and [Assignment: organization-defined personnel or roles] within:
* [Assignment: organization-defined time period] when accounts are no longer required;
* [Assignment: organization-defined time period] when users are terminated or transferred; and
* [Assignment: organization-defined time period] when system usage or need-to-know changes for an individual;
* Authorize access to the system based on:
* A valid access authorization;
* Intended system usage; and
* [Assignment: organization-defined attributes (as required)];
* Review accounts for compliance with account management requirements [Assignment: organization-defined frequency];
* Establish and implement a process for changing shared or group account authenticators (if deployed) when individuals are removed from the group; and
* Align account management processes with personnel termination and transfer processes.

# Supplemental Guidance
Examples of system account types include individual, shared, group, system, guest, anonymous, emergency, developer, temporary, and service. Identification of authorized system users and the specification of access privileges reflect the requirements in other controls in the security plan. Users requiring administrative privileges on system accounts receive additional scrutiny by organizational personnel responsible for approving such accounts and privileged access, including system owner, mission or business owner, senior agency information security officer, or senior agency official for privacy. Types of accounts that organizations may wish to prohibit due to increased risk include shared, group, emergency, anonymous, temporary, and guest accounts.

Where access involves personally identifiable information, security programs collaborate with the senior agency official for privacy to establish the specific conditions for group and role membership; specify authorized users, group and role membership, and access authorizations for each account; and create, adjust, or remove system accounts in accordance with organizational policies. Policies can include such information as account expiration dates or other factors that trigger the disabling of accounts. Organizations may choose to define access privileges or other attributes by account, type of account, or a combination of the two. Examples of other attributes required for authorizing access include restrictions on time of day, day of week, and point of origin. In defining other system account attributes, organizations consider system-related requirements and mission/business requirements. Failure to consider these factors could affect system availability.

Temporary and emergency accounts are intended for short-term use. Organizations establish temporary accounts as part of normal account activation procedures when there is a need for short-term accounts without the demand for immediacy in account activation. Organizations establish emergency accounts in response to crisis situations and with the need for rapid account activation. Therefore, emergency account activation may bypass normal account authorization processes. Emergency and temporary accounts are not to be confused with infrequently used accounts, including local logon accounts used for special tasks or when network resources are unavailable (may also be known as accounts of last resort). Such accounts remain available and are not subject to automatic disabling or removal dates. Conditions for disabling or deactivating accounts include when shared/group, emergency, or temporary accounts are no longer required and when individuals are transferred or terminated. Changing shared/group authenticators when members leave the group is intended to ensure that former group members do not retain access to the shared or group account. Some types of system accounts may require specialized training.

# Control Enhancements/Sub-Controls
AC-2(1): Automated System Account Management
Baseline(s):ModerateHigh
Support the management of system accounts using [Assignment: organization-defined automated mechanisms].

AC-2(2): Automated Temporary and Emergency Account Management
Baseline(s):ModerateHigh
Automatically [Assignment: remove, disable] temporary and emergency accounts after [Assignment: organization-defined time period].

AC-2(3): Disable Accounts
Baseline(s):ModerateHigh
Disable accounts within [Assignment: organization-defined time period] when the accounts: Have expired; Are no longer associated with a user or individual; Are in violation of organizational policy; or Have been inactive for [Assignment: organization-defined time period].

AC-2(4): Automated Audit Actions
Baseline(s):ModerateHigh
Automatically audit account creation, modification, enabling, disabling, and removal actions.

AC-2(5): Inactivity Logout
Baseline(s):ModerateHigh
Require that users log out when [Assignment: organization-defined time period of expected inactivity or description of when to log out].

AC-2(6): Dynamic Privilege Management
Baseline(s):(Not part of any baseline)
Implement [Assignment: organization-defined dynamic privilege management capabilities].

AC-2(7): Privileged User Accounts
Baseline(s):(Not part of any baseline)
Establish and administer privileged user accounts in accordance with [Assignment: a role-based access scheme, an attribute-based access scheme]; Monitor privileged role or attribute assignments; Monitor changes to roles or attributes; and Revoke access when privileged role or attribute assignments are no longer appropriate.

AC-2(8): Dynamic Account Management
Baseline(s):(Not part of any baseline)
Create, activate, manage, and deactivate [Assignment: organization-defined system accounts] dynamically.

AC-2(9): Restrictions on Use of Shared and Group Accounts
Baseline(s):(Not part of any baseline)
Only permit the use of shared and group accounts that meet [Assignment: organization-defined conditions].

AC-2(11): Usage Conditions
Baseline(s):High
Enforce [Assignment: organization-defined circumstances and/or usage conditions] for [Assignment: organization-defined system accounts].

AC-2(12): Account Monitoring for Atypical Usage
Baseline(s):High
Monitor system accounts for [Assignment: organization-defined atypical usage] ; and Report atypical usage of system accounts to [Assignment: organization-defined personnel or roles].

AC-2(13): Disable Accounts for High-risk Individuals
Baseline(s):ModerateHigh
Disable accounts of individuals within [Assignment: organization-defined time period] of discovery of [Assignment: organization-defined significant risks].

# Mappings
## SOC 2

```json_cmap
["CC1.1"]
```
## ISO 27001

```json_cmap
["A.1.1.1", "A.1.2.2"]
```

# Programatic Data
## System-level tests
### Okta
Date last testing: 2025-01-03
Test of Design: Effective
Test of Operating Effectiveness: Ineffective
Link to issue(s): url.com
System control maturity/health score: 2
#### Automations
##### Automation 1
Percentage of overall control: 30%
Link to script(s): url.com
Date of last successful test: yyyy-mm-dd (auttomated by above script)
Last test result: pass
##### Automation 2
Percentage of overall control: 10%
Link to script(s): url.com
Date of last successful test: yyyy-mm-dd (auttomated by above script)
Last test result: fail
### AWS
Date last testing: 2025-01-13
Test of Design: Effective
Test of Operating Effectiveness: Effective
Link to issue(s): 
System control maturity/health score: 4
#### Automations
##### Automation 1
Percentage of overall control: 40%
Link to script(s): url.com
Date of last successful test: yyyy-mm-dd (auttomated by above script)
Last test result: pass
##### Automation 2
Percentage of overall control: 30%
Link to script(s): url.com
Date of last successful test: yyyy-mm-dd (auttomated by above script)
Last test result: pass