## Purpose
The goal of this area is to create individual json files for each "active" security control currently deployed by an organization.

## Data Flows

```mermaid
graph TD;
    A["Security Control Files"]-->B["System Files"];
    A-->I[Frameworks];
    A-->C["Charting and Data"];
    A-->D["Findings/Observations/Issues"];
    C-->E["System Risk Information"];
    C-->F["Control Domain Risk Informatioon"];
    C-->G["Averaged Control Risk Information"];
    D-->C;
    C-->H["Findings/Observation/Issues Metrics"];
```