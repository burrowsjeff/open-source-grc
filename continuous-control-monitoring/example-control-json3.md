# Control Family
Access Control

# Control ID
AC-05

# Control Statement
* Identify and document [Assignment: organization-defined duties of individuals] ; and
* Define system access authorizations to support separation of duties.

# Supplemental Guidance
Separation of duties addresses the potential for abuse of authorized privileges and helps to reduce the risk of malevolent activity without collusion. Separation of duties includes dividing mission or business functions and support functions among different individuals or roles, conducting system support functions with different individuals, and ensuring that security personnel who administer access control functions do not also administer audit functions. Because separation of duty violations can span systems and application domains, organizations consider the entirety of systems and system components when developing policy on separation of duties. Separation of duties is enforced through the account management activities in AC-2 , access control mechanisms in AC-3 , and identity management activities in IA-2, IA-4 , and IA-12.

# Control Enhancements/Sub-Controls
N/A

# Mappings
## ISO 27001

```json_cmap
["A.2.1.2"]
```

# Programatic Data
## System-level tests
### Okta
Date last testing: 2025-01-03
Test of Design: Effective
Test of Operating Effectiveness: Ineffective
Link to issue(s): url.com
System control maturity/health score: 2
#### Automations
##### Automation 1
Percentage of overall control: 30%
Link to script(s): url.com
Date of last successful test: yyyy-mm-dd (auttomated by above script)
Last test result: pass
##### Automation 2
Percentage of overall control: 10%
Link to script(s): url.com
Date of last successful test: yyyy-mm-dd (auttomated by above script)
Last test result: fail
### AWS
Date last testing: 2025-01-13
Test of Design: Effective
Test of Operating Effectiveness: Effective
Link to issue(s): 
System control maturity/health score: 4
#### Automations
##### Automation 1
Percentage of overall control: 40%
Link to script(s): url.com
Date of last successful test: yyyy-mm-dd (auttomated by above script)
Last test result: pass
##### Automation 2
Percentage of overall control: 30%
Link to script(s): url.com
Date of last successful test: yyyy-mm-dd (auttomated by above script)
Last test result: pass