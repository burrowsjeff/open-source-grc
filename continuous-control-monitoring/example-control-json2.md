# Control Family
Access Control

# Control ID
AC-03

# Control Statement
Enforce approved authorizations for logical access to information and system resources in accordance with applicable access control policies.

# Supplemental Guidance
Access control policies control access between active entities or subjects (i.e., users or processes acting on behalf of users) and passive entities or objects (i.e., devices, files, records, domains) in organizational systems. In addition to enforcing authorized access at the system level and recognizing that systems can host many applications and services in support of mission and business functions, access enforcement mechanisms can also be employed at the application and service level to provide increased information security and privacy. In contrast to logical access controls that are implemented within the system, physical access controls are addressed by the controls in the Physical and Environmental Protection ( PE ) family.

# Control Enhancements/Sub-Controls
AC-3(2): Dual Authorization
Baseline(s):(Not part of any baseline)
Enforce dual authorization for [Assignment: organization-defined privileged commands and/or other actions].

AC-3(3): Mandatory Access Control
Baseline(s):(Not part of any baseline)
Enforce [Assignment: organization-defined mandatory access control policy] over the set of covered subjects and objects specified in the policy, and where the policy: Is uniformly enforced across the covered subjects and objects within the system; Specifies that a subject that has been granted access to information is constrained from doing any of the following; Passing…

AC-3(4): Discretionary Access Control
Baseline(s):(Not part of any baseline)
Enforce [Assignment: organization-defined discretionary access control policy] over the set of covered subjects and objects specified in the policy, and where the policy specifies that a subject that has been granted access to information can do one or more of the following: Pass the information to any other subjects or objects; Grant its privileges to…

AC-3(5): Security-relevant Information
Baseline(s):(Not part of any baseline)
Prevent access to [Assignment: organization-defined security-relevant information] except during secure, non-operable system states.

AC-3(7): Role-based Access Control
Baseline(s):(Not part of any baseline)
Enforce a role-based access control policy over defined subjects and objects and control access based upon [Assignment: organization-defined roles and users authorized to assume such roles].

AC-3(8): Revocation of Access Authorizations
Baseline(s):(Not part of any baseline)
Enforce the revocation of access authorizations resulting from changes to the security attributes of subjects and objects based on [Assignment: organization-defined rules].

AC-3(9): Controlled Release
Baseline(s):(Not part of any baseline)
Release information outside of the system only if: The receiving [Assignment: organization-defined system or system component] provides [Assignment: organization-defined controls] ; and [Assignment: organization-defined controls] are used to validate the appropriateness of the information designated for release.

AC-3(10): Audited Override of Access Control Mechanisms
Baseline(s):(Not part of any baseline)
Employ an audited override of automated access control mechanisms under [Assignment: organization-defined conditions] by [Assignment: organization-defined roles].

AC-3(11): Restrict Access to Specific Information Types
Baseline(s):(Not part of any baseline)
Restrict access to data repositories containing [Assignment: organization-defined information types].

AC-3(12): Assert and Enforce Application Access
Baseline(s):(Not part of any baseline)
Require applications to assert, as part of the installation process, the access needed to the following system applications and functions: [Assignment: organization-defined system applications and functions]; Provide an enforcement mechanism to prevent unauthorized access; and Approve access changes after initial installation of the application.

AC-3(13): Attribute-based Access Control
Baseline(s):(Not part of any baseline)
Enforce attribute-based access control policy over defined subjects and objects and control access based upon [Assignment: organization-defined attributes].

AC-3(14): Individual Access
Baseline(s):Privacy
Provide [Assignment: organization-defined mechanisms] to enable individuals to have access to the following elements of their personally identifiable information: [Assignment: organization-defined elements].

AC-3(15): Discretionary and Mandatory Access Control
Baseline(s):(Not part of any baseline)
Enforce [Assignment: organization-defined mandatory access control policy] over the set of covered subjects and objects specified in the policy; and Enforce [Assignment: organization-defined discretionary access control policy] over the set of covered subjects and objects specified in the policy.

# Mappings
## SOC 2

```json_cmap
    ["CC5.2"]
```

# Programatic Data
## System-level tests
### Okta
Date last testing: 2025-01-03
Test of Design: Effective
Test of Operating Effectiveness: Ineffective
Link to issue(s): url.com
System control maturity/health score: 2
#### Automations
##### Automation 1
Percentage of overall control: 30%
Link to script(s): url.com
Date of last successful test: yyyy-mm-dd (auttomated by above script)
Last test result: pass
##### Automation 2
Percentage of overall control: 10%
Link to script(s): url.com
Date of last successful test: yyyy-mm-dd (auttomated by above script)
Last test result: fail
### AWS
Date last testing: 2025-01-13
Test of Design: Effective
Test of Operating Effectiveness: Effective
Link to issue(s): 
System control maturity/health score: 4
#### Automations
##### Automation 1
Percentage of overall control: 40%
Link to script(s): url.com
Date of last successful test: yyyy-mm-dd (auttomated by above script)
Last test result: pass
##### Automation 2
Percentage of overall control: 30%
Link to script(s): url.com
Date of last successful test: yyyy-mm-dd (auttomated by above script)
Last test result: pass